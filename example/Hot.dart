import 'Menu.dart';

class Hot extends Menu {
  @override
  var price = [
    30,
    35,
    30,
  ];
  @override
  var menu = [
    'HOT CAFE LATTE',
    'HOT CAPUCCINO',
    'HOT MATCHA LATTE',
  ];
}
