abstract class Menu {
  var price = [];
  var menu = [];

  String getMenu(int index) => menu[index];
  int getPrice(int index) => price[index];
  String printMenu() {
    for (int i = 0; i < menu.length; i++) {
      print('${getMenu(i)}        INPUT     ${i + 1}');
    }
    return "Menu error";
  }
}
