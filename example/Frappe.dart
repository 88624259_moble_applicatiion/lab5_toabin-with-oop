import 'ICE.dart';
import 'Menu.dart';

class Frappe extends Ice {
  @override
  var menu = [
    'FRAPPE CALAMEL CAFELATTE',
    'FRAPPE TAWANESE TEA CAFFE LATTE',
    'FRAPPE ESPRESSO',
    'FRAPPE THAI MILK TEA',
    'FRAPPE TEA',
    'FRAPPE LIMEADE TEA'
  ];
  @override
  var price = [45, 50, 45, 55, 60, 45];
  @override
  String printMenu() {
    for (int i = 0; i < menu.length; i++) {
      print('${getMenu(i)}        INPUT     ${i + 10}');
    }
    return "Menu error";
  }
}
