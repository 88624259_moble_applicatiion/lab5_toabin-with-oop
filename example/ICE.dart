import 'Menu.dart';

class Ice extends Menu {
  @override
  var price = [40, 45, 40, 50, 55, 40];
  var menuTopping = ['BROWN SUGAR', 'BROWA HONEY', 'FRUIT SALAD'];
  var priceTopping = [20, 15, 10];
  @override
  var menu = [
    'ICED CALAMEL CAFELATTE',
    'ICED TAWANESE TEA CAFFE LATTE',
    'ICED ESPRESSO',
    'ICED THAI MILK TEA',
    'ICED TEA',
    'ICED LIMEADE TEA'
  ];

  String getMenuTopping(int index) => menuTopping[index];
  int getPriceTopping(int index) => priceTopping[index];
  @override
  String printMenu() {
    for (int i = 0; i < menu.length; i++) {
      print('${getMenu(i)}        INPUT     ${i + 4}');
    }
    return "Menu error";
  }

  String printTopping() {
    print('PLEASE SELECT TOPPING');
    for (int i = 0; i < menuTopping.length; i++) {
      print('${getMenuTopping(i)}        INPUT     ${i + 1}');
    }
    return "IF YOU DON'T WANT THE TOPPING     INPUT    5 ";
  }
}
