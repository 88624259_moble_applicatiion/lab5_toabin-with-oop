import 'dart:io';
import 'Frappe.dart';
import 'Hot.dart';
import 'ICE.dart';

class Toabin {
  var ice = Ice();
  var hot = Hot();
  var frappe = Frappe();
  int? numMenu;
  var menu;
  int price = 0;
  String? payment;

  var listpayment = ['cash', 'QR code', 'credit card', 'rabbit pay'];

  void selectMenu(int setNumMenu) {
    numMenu = setNumMenu;
    menu = setMenu(numMenu!, menu);
  }

  Object setMenu(int nunMenu, var menu) {
    if (numMenu == null) return false;
    if (numMenu! > 8) {
      menu = Frappe().getMenu(nunMenu - 9);
      price = Frappe().price[nunMenu - 9];
    } else if (numMenu! > 2) {
      menu = Ice().getMenu(nunMenu - 3);
      price = Ice().price[nunMenu - 3];
    } else {
      menu = Hot().getMenu(nunMenu);
      price = Hot().price[nunMenu];
    }
    return menu;
  }

  void printPay() {
    for (int i = 0; i < listpayment.length; i++) {
      stdout.write('${listpayment[i]}   INPUT   ${i + 1} \n');
    }
  }

  void setPayment(int num) {
    payment = listpayment[num - 1];
  }

  void printMenu() {
    hot.printMenu();
    ice.printMenu();
    frappe.printMenu();
  }

  String checkMenu(String menu) {
    if (menu.substring(0, 4) == 'ICED' || menu.substring(0, 6) == 'FRAPPE') {
      print(ice.printTopping());
      return selectTopping(int.parse(stdin.readLineSync()!) - 1);
    }
    return '';
  }

  String selectTopping(int numTopping) {
    if (checkNoTopping(numTopping) == true) {
      return 'MENU OF YOUR SELECT IS  " $menu " ';
    }
    price += ice.priceTopping[numTopping];
    return 'MENU OF YOUR SELECT IS " $menu " THE TOPPING IS "${ice.menuTopping[numTopping]}"';
  }

  bool checkNoTopping(int num) {
    if (num > ice.menuTopping.length) return true;
    return false;
  }
}
